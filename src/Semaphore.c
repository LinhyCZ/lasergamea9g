#include "Semaphore.h"

/* ____________________________________________________________________________

    void CreateSemaphore(uint8_t * sem)

    Sets given semaphore value to 0.
   ____________________________________________________________________________
*/
void CreateSemaphore(uint8_t * sem) {
    *sem = 0;
}

/* ____________________________________________________________________________

    void CreateSemaphore(uint8_t * sem)

    Sets given semaphore value to 1.
   ____________________________________________________________________________
*/
void ReleaseSemaphore(uint8_t * sem) {
    *sem = 1;
}

/* ____________________________________________________________________________

    void CreateSemaphore(uint8_t * sem)

    Waits until given semaphore value is 0.
   ____________________________________________________________________________
*/
void WaitSemaphore(uint8_t * sem) {
    while (*sem == 0) {
        OS_Sleep(1);
    }
        
    *sem = 0;
}