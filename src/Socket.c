#include "Socket.h"

int * errorCodePointer;
uint8_t socketSemaphore = 0;                    //Semaphore value
int socketFd = -1;                              //Socket file descriptor
uint8_t buffer[RECEIVE_BUFFER_MAX_LENGTH];      //Message buffer

/* ____________________________________________________________________________

    uint8_t * getSocketSemaphore()

    Returns pointer to semaphore value.
   ____________________________________________________________________________
*/
uint8_t * getSocketSemaphore() {
    return &socketSemaphore;
}

/* ____________________________________________________________________________

    bool Connect(int * errorCode) 

    Connects to socket server. Returns true if connection was successful, false
    oterwise.
   ____________________________________________________________________________
*/
bool Connect(int * errorCode) {
    Trace(1,"SOCKET: Starting connection to server.");
    memset(buffer, 0, sizeof(buffer)); //Empty the buffer

    if (DNS_GetHostByName2(DNS_DOMAIN, (char*) buffer) != 0) {
        Trace(3, "DNS: Failed to find IP value for DNS name: %s", DNS_DOMAIN);
        return false;
    }

    Trace(2,"DNS: Found IP %s for domain: %s", DNS_DOMAIN, buffer);

    CreateSemaphore(getSocketSemaphore());
    socketFd = Socket_TcpipConnect(TCP, buffer, SERVER_PORT);
    Trace(2,"SOCKET: starting to connect to server, socketFd: %d",socketFd);

    WaitSemaphore(getSocketSemaphore());

    if (errorCode == NULL) {
        errorCode = errorCodePointer;       //Use stored pointer
    } else {
        errorCodePointer = errorCode;       //Store pointer in variable
    }

    if(*errorCode != 0) {
        *errorCode = 0;
        Trace(2,"SOCKET: Error ocurred");
        Socket_TcpipClose(socketFd);
        return false;
    }
    
    Trace(2,"SOCKET: Connection complete.");

    return true;
}

/* ____________________________________________________________________________

    void Close()

    Closes socket.
   ____________________________________________________________________________
*/
void Close() {
    CreateSemaphore(getSocketSemaphore());;
    Socket_TcpipClose(socketFd);
    WaitSemaphore(getSocketSemaphore());
}


/* ____________________________________________________________________________

    bool Write(uint8_t* data, uint16_t len)

    Sends given data to socket server. Return true if message was sent successfuly,
    false otherwise.
   ____________________________________________________________________________
*/
bool Write(uint8_t* data, uint16_t len) {
    Trace(2,"SOCKET: Writing value");
    
    CreateSemaphore(getSocketSemaphore());
    int error = Socket_TcpipWrite(socketFd, data, len);
    if (error <= 0) {
        Trace(2,"SOCKET: Write fail: %d", error);
        return false;
    }    

    Trace(2,"SOCKET: %d is sending %d bytes data to server: %s, error: %d", socketFd, len, data, error);

    return true;
}


/* ____________________________________________________________________________

    void writeInt(int val)

    Sends int to socket.
   ____________________________________________________________________________
*/
void writeInt(int val) {
    //Prepare int char array
    char s[13]; 
    memset(s,0,sizeof(s));
    sprintf(s,"%d\n", val);

    WriteWithCheck((uint8_t *) s);
}

/* ____________________________________________________________________________

    void writeCharArray(uint8_t * data)

    Sends char array to socket.
   ____________________________________________________________________________
*/
void writeCharArray(uint8_t * data) {
    Trace(1, "Trying to send char array");
    WriteWithCheck(data);
}

/* ____________________________________________________________________________

    void WriteWithCheck(uint8_t* data)

    Keeps sending message to server until message is transfered successfuly.
    If connection gets closed, it keeps reconnecting and then sends message.
   ____________________________________________________________________________
*/
void WriteWithCheck(uint8_t* data) {
    bool messageSent = false;
    int failCount = 0;

    while(!messageSent)
    {
        if (failCount == 5) {
            // Close socket so it can be started again.
            Close();
        }
        
        if (failCount >= 5) {
            if (Connect(NULL)) {
                //If connection successful reset fail counter.
                failCount = 0;
            } else {
                //If reconnection failed, add fail count. We don't need to close the socket, since it wasn't connected yet.
                ++failCount;
            }
        } else {
            if (!Write(data, strlen(data))) {
                ++failCount;
                Trace(2,"SOCKET: Write fail, fail count: %d", failCount);
            } else {
                Trace(2,"SOCKET: Write success, fail count: %d", failCount);
                messageSent = true;
            }
        }

        //Give some time before retrying.
        OS_Sleep(500);
    }
}
// Write part END

/* ____________________________________________________________________________

    void sendAck(int MessageID)

    Sends ACK_[MESSAGEID] message to socket server.
   ____________________________________________________________________________
*/
void sendAck(int MessageID) {
    uint8_t ACK_BUFFER_SIZE = 15;
    char * Socket_ackBuffer = OS_Malloc(ACK_BUFFER_SIZE);

    if (Socket_ackBuffer == NULL) {
        Trace(1,"Couldn't allocate enough memory for Socket message ACK buffer.");
        return;
    }

    //Zero the buffer and write ACK response into it.
    memset(Socket_ackBuffer, 0, ACK_BUFFER_SIZE);
    sprintf(Socket_ackBuffer, ACK_MESSAGE, MessageID);

    //Send ACK to server.
    writeCharArray(Socket_ackBuffer);

    OS_Free(Socket_ackBuffer);
}

/* ____________________________________________________________________________

    char * receive(API_Event_t* pEvent)

    Event for processing received messages from socket server. Sends acknowledge 
    message and returns parsed data without message ID in the beginning. Returns
    NULL when received PING message from the server.
   ____________________________________________________________________________
*/
char * receive(API_Event_t* pEvent) {
    int fd = pEvent->param1;
    int length = (pEvent->param2 > RECEIVE_BUFFER_MAX_LENGTH ? RECEIVE_BUFFER_MAX_LENGTH : pEvent->param2);
    
    memset(buffer, 0, sizeof(buffer));
    length = Socket_TcpipRead(fd, buffer, length);

    Trace(2,"SOCKET: %d received %d bytes data:%s",fd,length,buffer);

    char * bufferChar = (char *) buffer;
    if (strncmp(bufferChar, SOCKET_PING_MESSAGE, strlen(SOCKET_PING_MESSAGE)) == 0) {
        Trace(1, "Received PING message");
        //Ignore ping messages because they are just for the server to realize the connection is closed.
        return NULL;
    }
    char * IDPartChar = strsep(&bufferChar, "_");
    
    int MessageID;
    sscanf(IDPartChar, "%d", &MessageID);

    sendAck(MessageID);

    Trace(2,"SOCKET: Received message ID:%d",MessageID);
    return bufferChar;
}

// Receive part END
