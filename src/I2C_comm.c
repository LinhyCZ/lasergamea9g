#include "I2C_comm.h"

uint8_t sensorsConnected = 0;
uint8_t sensorAddressess[MAX_SENSORS];

uint8_t I2CSemaphore = 1; //Semaphore is open by default

/* ____________________________________________________________________________

    void initializeI2C()

    This function initializes I2C bus of the device.
   ____________________________________________________________________________
*/
void initializeI2C() {
    I2C_Config_t config;

    config.freq = I2C_FREQ_100K;
    I2C_Init(I2C_PORT, config);
}

/* ____________________________________________________________________________

    void scanForSensors()

    This function scans for LaserGame sensors using Hello/HelloResp commands 
    connected to the I2C bus and stores them in global array.
   ____________________________________________________________________________
*/
void scanForSensors() {
    /* Initialize RECEIVE message buffer */
    uint8_t * I2C_ReceiveBuffer = OS_Malloc(I2C_BUFFER_MAX_LENGTH);

    if (I2C_ReceiveBuffer == NULL) {
        Trace(1,"Couldn't allocate enough memory for I2C Scan Receive buffer.");
        return;
    }

    for (uint8_t i = 0; i < MAX_SENSORS; i++) {
        uint8_t currentSensorAddress = I2C_SENSOR_START_ADDRESS + i;

        for (uint8_t j = 0; j < I2C_SENSOR_SCAN_MAX_RETRIES; j++) {
            sendByte(HELLO, currentSensorAddress);

            memset(I2C_ReceiveBuffer, 0, I2C_BUFFER_MAX_LENGTH);
            I2C_Error_t error = readMessage(I2C_ReceiveBuffer, currentSensorAddress);

            //If received correct response break out of trying to read the value and continue with other sensor
            if (error == 0 && I2C_ReceiveBuffer[0] == HELLO_RESPONSE) {
                sensorAddressess[sensorsConnected] = currentSensorAddress;
                sensorsConnected++;
                break;
            }

            OS_Sleep(I2C_SENSOR_SCAN_DELAY);
        }
    }

    Trace(1, "Number of connected sensors %d", sensorsConnected);

    for (int i = 0; i < sensorsConnected; i++) {
        Trace(1, "Address of sensor No. %d : %d", i, sensorAddressess[i]);
    }

    OS_Free(I2C_ReceiveBuffer);
}

/* ____________________________________________________________________________

    I2C_Error_t sendByte(uint8_t dataByte, uint8_t address)

    This function sends one byte of data to given address. Returns error 
    code that occured while sending data.
   ____________________________________________________________________________
*/
I2C_Error_t sendByte(uint8_t dataByte, uint8_t address) {
    return sendMessage(&dataByte, 1, address);
}

/* ____________________________________________________________________________

    I2C_Error_t sendMessage(uint8_t* message, uint16_t length, uint8_t address)

    This function sends message of given length to given address. Returns error 
    code that occured while sending data.
   ____________________________________________________________________________
*/
I2C_Error_t sendMessage(uint8_t* message, uint16_t length, uint8_t address) {
    WaitSemaphore(&I2CSemaphore); //Wait until other messages are sent
    CreateSemaphore(&I2CSemaphore); //Claim the bus for myself
    
    I2C_Error_t sendError = I2C_WriteMem(I2C_PORT, address, 0x00, 1, message, length, I2C_TIMEOUT_MS);

    ReleaseSemaphore(&I2CSemaphore); //Done, open the semaphore
    return sendError;
}

/* ____________________________________________________________________________

    void trimI2CMessage(uint8_t * I2Cbuffer)

    This function trims received message in the I2C buffer. When reading of 
    the values ends, value 0xFF is set in the rest of the buffer. To work
    correctly, 0xFF value is replace with 0x00.
   ____________________________________________________________________________
*/
void trimI2CMessage(uint8_t * I2Cbuffer) {
    bool foundEndOfMessage = false;

    if (I2Cbuffer != NULL) {
        for (uint8_t i = 0; i < I2C_BUFFER_MAX_LENGTH; i++) {
        if (I2Cbuffer[i] == 0xFF) {
            foundEndOfMessage = true;
        }

        if (foundEndOfMessage) {
            I2Cbuffer[i] = 0x00;
        }
    }
    }
}

/* ____________________________________________________________________________

    I2C_Error_t readMessage(uint8_t *buffer, uint8_t address)

    This function tries to read a message from given address into the buffer.
    Returns error code that occured while receiving data.
   ____________________________________________________________________________
*/
I2C_Error_t readMessage(uint8_t *buffer, uint8_t address) {
    memset(buffer, 0, I2C_BUFFER_MAX_LENGTH);

    WaitSemaphore(&I2CSemaphore); //Wait until other messages are received
    CreateSemaphore(&I2CSemaphore); //Claim the bus for myself

    I2C_Error_t errReceive = I2C_ReadMem(I2C_PORT, address, 0x00, 1, buffer, I2C_BUFFER_MAX_LENGTH, I2C_TIMEOUT_MS);
    
    ReleaseSemaphore(&I2CSemaphore); //Done, open the semaphore

    Trace(1,"I2C Read error: %d",errReceive);
    
    if (errReceive == I2C_ERROR_NONE) {
        trimI2CMessage(buffer);
    } 

    return errReceive;
}

/* ____________________________________________________________________________

    bool readMessage(uint8_t *buffer, uint8_t address)

    This function tries to read a message from all devices connected to the vest.
    If device returns NOINFO or timeouts, it continues to other device.
    Returns true if all devices were asked, false if not and there might be message in other device waiting.
   ____________________________________________________________________________
*/
bool readMessageFromAllDevices(uint8_t *buffer) {
    I2C_Error_t error = readMessage(buffer, I2C_WEAPON_ADDRESS);
    if (error == I2C_ERROR_NONE && buffer[0] != NOINFO) {
        return false;
    }
    
    for (uint8_t i = 0; i < sensorsConnected; i++) {
        error = readMessage(buffer, sensorAddressess[i]);

        if (error == I2C_ERROR_NONE && buffer[0] != NOINFO) {
            if (i == sensorsConnected - 1) {
                return true;
            }

            return false;
        }
    }

    return true; // Read from all sensors, didn't find anything.
}


/* ____________________________________________________________________________

    void sendMessageToWeapon(uint8_t * data, uint16_t len)

    Sends given data of given length to weapon.
   ____________________________________________________________________________
*/
void sendMessageToWeapon(uint8_t * data, uint16_t len) {
    sendMessage(data, len, I2C_WEAPON_ADDRESS);
}


/* ____________________________________________________________________________

    void sendByteToWeapon(uint8_t data)

    Sends given byte to weapon.
   ____________________________________________________________________________
*/
void sendByteToWeapon(uint8_t data) {
    sendByte(data, I2C_WEAPON_ADDRESS);
}

/* ____________________________________________________________________________

    void broadcastMessageToSensors(uint8_t * data, uint16_t len)

    Sends given data of given length to all sensors.
   ____________________________________________________________________________
*/
void broadcastMessageToSensors(uint8_t * data, uint16_t len) {
    for (uint8_t i = 0; i < sensorsConnected; i++) {
        sendMessage(data, len, sensorAddressess[i]);
    }
}

/* ____________________________________________________________________________

    void broadcastByteToSensors(uint8_t data) 

    Sends given byte to all sensors.
   ____________________________________________________________________________
*/
void broadcastByteToSensors(uint8_t data) {
    for (uint8_t i = 0; i < sensorsConnected; i++) {
        sendByte(data, sensorAddressess[i]);
    }
}

/* ____________________________________________________________________________

    void broadcastMessageToSensorsAndWeapon(uint8_t * data, uint16_t len)

    Sends given data of given length to all sensors and weapon devices.
   ____________________________________________________________________________
*/
void broadcastMessageToSensorsAndWeapon(uint8_t * data, uint16_t len) {
    sendMessageToWeapon(data, len);
    broadcastMessageToSensors(data, len);
} 

/* ____________________________________________________________________________

    void broadcastByteToSensorsAndWeapon(uint8_t data)

    Sends given byte to all sensors and weapon devices.
   ____________________________________________________________________________
*/
void broadcastByteToSensorsAndWeapon(uint8_t data) {
    sendByteToWeapon(data);
    broadcastByteToSensors(data);
} 