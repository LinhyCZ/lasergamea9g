#include "Game.h"

uint8_t team = 0;                                   //Team ID of the current player. Is set by PREPARE message.
uint8_t playerID = 0;                               //ID of the current player. Is set by PREPARE message.

uint8_t fullAmmoAmout = 30;                         //Full amount of ammo. Is set by START message
uint8_t fullHealthAmout = 100;                      //Full health of the player. Is set by START message
uint8_t damage = 30;                                //Damage by one shot. Is set by START message
uint16_t respawnPeriod = 0;                         //Respawn period that is set by START message

uint8_t health = 0;                                 //Current health of player
uint8_t ammo = 0;                                   //Current amount of ammo player has in magazine

uint8_t shotBy = 0;                                 //ID of the player who killed this player.

uint8_t shotByMessageShownCounter = 0;              //Counter to show shot by message. Default value is 0
int8_t endOfGameMessageShownCounter = -1;           //Counter to show end of game message. Default value is -1

uint32_t gameStartTime = 0;                         //Epoch time when the game starts.
uint32_t gameEndTime = 0;                           //Epoch time when the game ends.

uint32_t killTime = 0;                              //Epoch time of player death.

uint8_t numberOfPlayers = 0;                        //Number of players in the game.
Player players[MAX_PLAYERS];                        //Array to hold all players.

uint8_t gpsCounter = 0;                             //Counter to send GPS coordinates.
uint8_t displayReinitializeCounter = 0;             //Counter to reinitialize Display.

bool isLoggedInVar = false;                         //Flag if vest is logged in.

bool isStarted = false;                             //Flag if game is started.

bool loopInterrupt = false;                         //Flag if loop should be interrupted.
uint8_t interruptReason = INTERRUPT_REASON_NONE;    //Interrupt reason. I2C(sensor) or Shoot interrupt.


/* ____________________________________________________________________________

    void PlayCallback(AUDIO_Error_t result)

    Default audio play callback.
   ____________________________________________________________________________
*/
void PlayCallback(AUDIO_Error_t result) {
    Trace(1,"play callback result:%d",result);
}

/* ____________________________________________________________________________

    void playAudio(char * path)

    Plays audio from given path.
   ____________________________________________________________________________
*/
void playAudio(char * path) {
    AUDIO_SetMode(AUDIO_MODE_LOUDSPEAKER);
    AUDIO_Error_t ret = AUDIO_Play(path, AUDIO_TYPE_MP3, PlayCallback);
    Trace(1, "Audio err: %d", ret);
}

/* ____________________________________________________________________________

    void start()

    Starts the game. Activates sensors and weapons and sets value by calling respawn.
   ____________________________________________________________________________
*/
void start() {
    isStarted = true;
    broadcastByteToSensorsAndWeapon(ACTIVATE);
    respawn();
    gameRedrawDisplay();
}

/* ____________________________________________________________________________

    void stop()

    Stops the game. Resets values to default and deactivates sensors and weapon.
   ____________________________________________________________________________
*/
void stop() {
    killTime = 0;
    gameEndTime = 0;
    isStarted = false;
    endOfGameMessageShownCounter = 0;
    broadcastByteToSensorsAndWeapon(DEACTIVATE); //Deactivate sensors
    gameRedrawDisplay();
}

/* ____________________________________________________________________________

    void initializeGame()

    Sends initialize command with game data to sensors and weapon.
   ____________________________________________________________________________
*/
void initializeGame() {
    uint16_t dataLength = 3;
    uint8_t data[3];
    memset(data, 0, dataLength);

    data[0] = INIT;
    data[1] = team;
    data[2] = playerID;

    broadcastMessageToSensorsAndWeapon(data, dataLength);
}

/* ____________________________________________________________________________

    void shootInterrupt()

    Breaks game loop and sets interrupt reason to shoot.
   ____________________________________________________________________________
*/
void shootInterrupt() {
    interruptReason = INTERRUPT_REASON_SHOOT;
    loopInterrupt = true;
}

/* ____________________________________________________________________________

    void shoot()

    Handles shoot interrupt action. Removes one from the ammo counter and sends 
    OUTOFAMMO message to the weapon if player is out of ammo. Plays empty magazine 
    audio if player is out of ammo. Plays shoot sound.
   ____________________________________________________________________________
*/
void shoot() {
    Trace(1, "Shooting.");
    if (!isStarted) {
        return;
    }

    if (ammo <= 0) {
        playAudio("/t/empty.mp3");
        return;
    }
    
    ammo--;

    if (ammo == 0) {
        sendByteToWeapon(OUTOFAMMO);
    }

    playAudio("/t/shoot.mp3");
}

/* ____________________________________________________________________________

    bool isLoggedIn()

    Returns true if vest is logged in, false otherwise.
   ____________________________________________________________________________
*/
bool isLoggedIn() {
    return isLoggedInVar;
}

/* ____________________________________________________________________________

    bool isAlive()

    Returns true if player is alive, false otherwise.
   ____________________________________________________________________________
*/
bool isAlive() {
    if (health > 0) {
        return true;
    }

    return false;
}

/* ____________________________________________________________________________

    bool addDamage(int shooter)

    Adds damage by player whos ID is in the parameter. Returns true if player
    is still alive, false if he is dead. If player is dead, sends message
    to socket server with information about the player who killed current player.
   ____________________________________________________________________________
*/
bool addDamage(int shooter) {
    shotBy = shooter;

    if (damage >= health) { //He is dead :(
        health = 0;
        killTime = getTime();

        //Send to socket information about kill
        char * buffer = OS_Malloc(30);
        
        if (buffer == NULL) {
            Trace(1,"Couldn't allocate enough memory for sending kill buffer.");
            return false;
        }

        sprintf(buffer, KILLED_BY_SOCKET_MESSAGE, shotBy);
        WriteWithCheck(buffer);
        OS_Free(buffer);

        return false;
    } 

    health -= damage;

    return true;
}

/* ____________________________________________________________________________

    void parseMessage(uint8_t * message)

    Parses message from I2C devices and does action according to the received 
    command.
   ____________________________________________________________________________
*/
void parseMessage(uint8_t * message) {
    Trace(1, "Parsing message: Command %x", message[0]);
    Trace(1, "Parsing message: Additional data %x", message[1]);
    switch (message[0]) {
        case SHOT: ;                                            //Add semicolon to fix a weird compiler issue: https://www.educative.io/edpresso/resolving-the-a-label-can-only-be-part-of-a-statement-error
            uint8_t shotPlayerId = message[1];

            if (shotPlayerId == playerID) {
                return;                                         //Don't allow to shoot myself
            }

            if (!isAlive() || !addDamage(shotPlayerId)) {       //First check if is not alive. In that case vest sensors probably didn't receive DEACTIVATE message, try it again.
                broadcastByteToSensorsAndWeapon(DEACTIVATE);      
            } 

            Trace(2, "GAME: Received shot from: %x", shotPlayerId);
            break;
        case RELOAD: ;
            ammo = fullAmmoAmout;
            sendByteToWeapon(RELOAD_COMPLETE);
            playAudio("/t/reload.mp3");
            break;
        case NOINFO: ;
            //Do nothing. Take a chill pill :)
            break;
        default: ;
            Trace(2, "GAME: Received unkown command from I2C: %x", message[0]);
            break;
    }
}

/* ____________________________________________________________________________

    void parseSocketMessage(char * message)

    Parses message from socket server and does action according to the received 
    command.
   ____________________________________________________________________________
*/
void parseSocketMessage(char * message) {
    char * responseBuffer = OS_Malloc(RECEIVE_BUFFER_MAX_LENGTH);
    
    if (responseBuffer == NULL) {
        Trace(1,"Couldn't allocate enough memory for parse socket message buffer.");
        return;
    }

    memset(responseBuffer, 0, RECEIVE_BUFFER_MAX_LENGTH);

    Trace(1, "Parsing message: %s", message);

    char * messagePart = strsep(&message, "_"); //Get the first command.

    
    if (strncmp(messagePart, SOCKET_WELCOME_MESSAGE, strlen(SOCKET_WELCOME_MESSAGE)) == 0) {
        sprintf(responseBuffer, LOGIN_MESSAGE, VEST_CODE);
        WriteWithCheck(responseBuffer);
    } else if (strncmp(messagePart, PREPARE_MESSAGE, strlen(PREPARE_MESSAGE)) == 0) {
        numberOfPlayers = 0;    //Initialize variable

        messagePart = strsep(&message, "_");
        sscanf(messagePart, "%hhd", &playerID);

        messagePart = strsep(&message, "_");
        sscanf(messagePart, "%hhd", &team);

        memset(players, 0, sizeof(Player) * MAX_PLAYERS);

        Trace(1, "Sizeof players: %hhd", sizeof(Player));

        while ((messagePart = strsep(&message, "_")) != NULL) {
            uint8_t newPlayerID;
            sscanf(messagePart, "%hhd", &newPlayerID);
            players[numberOfPlayers].playerId = newPlayerID;

            uint8_t teamID;
            sscanf(messagePart, "%hhd", &teamID);
            messagePart = strsep(&message, "_");
            players[numberOfPlayers].teamId = teamID;

            messagePart = strsep(&message, "_");
            sprintf(players[numberOfPlayers].username, "%s", messagePart);

            numberOfPlayers++;
        }


        for (uint8_t i = 0; i < numberOfPlayers; i++) {
            Trace(1, "Loaded player: %s", players[i].username);
        }

        Trace(1, "My team: %d", team);
        Trace(1, "My ID: %d", playerID);

        initializeGame();
        WriteWithCheck(PREPARE_DONE_MESSAGE);
    } else if (strncmp(messagePart, START_MESSAGE, strlen(START_MESSAGE)) == 0) {
        messagePart = strsep(&message, "_");
        sscanf(messagePart, "%hhd", &fullAmmoAmout);
        
        messagePart = strsep(&message, "_");
        sscanf(messagePart, "%hhd", &fullHealthAmout);

        messagePart = strsep(&message, "_");
        sscanf(messagePart, "%hhd", &damage);
        
        messagePart = strsep(&message, "_");
        sscanf(messagePart, "%hd", &respawnPeriod);
        
        messagePart = strsep(&message, "_");
        sscanf(messagePart, "%zu", &gameStartTime);
        
        messagePart = strsep(&message, "_");
        sscanf(messagePart, "%zu", &gameEndTime);

        WriteWithCheck(START_DONE_MESSAGE);
    } else if (strncmp(messagePart, LOGINOK_MESSAGE, strlen(LOGINOK_MESSAGE)) == 0) {
        isLoggedInVar = true;
        clear();
        showReadyToStartMessage();
    } else if (strncmp(messagePart, LOGINERR_MESSAGE, strlen(LOGINERR_MESSAGE)) == 0) {
        clear();
        showLoginFailedMessage();
    } else if (strncmp(messagePart, STOP_MESSAGE, strlen(STOP_MESSAGE)) == 0) {
        stop();
    } else {
        Trace(1, "Couldn't parse message. %s", messagePart);
    }

    OS_Free(responseBuffer);
}

/* ____________________________________________________________________________

    void respawn()

    Respawns the player. Resets some values to default values and sends ACTIVATE
    message to all I2C devices.
   ____________________________________________________________________________
*/
void respawn() {
    killTime = 0;
    health = fullHealthAmout;
    ammo = fullAmmoAmout;
    shotBy = 0;

    Trace(1, "Broadcasting activate message.");
    broadcastByteToSensorsAndWeapon(ACTIVATE);
    
    gameRedrawDisplay();
}

/* ____________________________________________________________________________

    Player * findPlayerById(uint8_t _playerID)

    Returns pointer to Player struct by given playerID. May return NULL if player
    with given ID doesn't exist
   ____________________________________________________________________________
*/
Player * findPlayerById(uint8_t _playerID) {
    for (uint8_t i = 0; i < numberOfPlayers; i++) {
        if (players[i].playerId == _playerID) {
            return &(players[i]);
        }
    }

    return NULL;
}


/* ____________________________________________________________________________

    void sendGpsInfo()

    If Chip is A9G (the one with GPS module) and GPS is open, it sends GPS 
    coordinates to Socket server in POS_[LAT]_[LON] message
   ____________________________________________________________________________
*/
void sendGpsInfo() {
    #if CHIP == A9G
    if (GPS_IsOpen()) {
        GPS_Info_t* gpsInfo = Gps_GetInfo();

        if (gpsCounter == GPS_SEND_INTERVAL) {
            gpsCounter = 0;

            int temp = (int)(gpsInfo->rmc.latitude.value/gpsInfo->rmc.latitude.scale/100);
            double latitude = temp+(double)(gpsInfo->rmc.latitude.value - temp*gpsInfo->rmc.latitude.scale*100)/gpsInfo->rmc.latitude.scale/60.0;
            temp = (int)(gpsInfo->rmc.longitude.value/gpsInfo->rmc.longitude.scale/100);
            double longitude = temp+(double)(gpsInfo->rmc.longitude.value - temp*gpsInfo->rmc.longitude.scale*100)/gpsInfo->rmc.longitude.scale/60.0;

            char * gpsMessageBuffer = OS_Malloc(RECEIVE_BUFFER_MAX_LENGTH);
            
            if (gpsMessageBuffer == NULL) {
                Trace(1,"Couldn't allocate enough memory for sending GPS message buffer.");
                return;
            }

            sprintf(gpsMessageBuffer, POSITION_MESSAGE, latitude, longitude);

            WriteWithCheck(gpsMessageBuffer);
            
            OS_Free(gpsMessageBuffer);
            return;
        }

        gpsCounter++;
    }
    #endif
}

/* ____________________________________________________________________________

    void interruptFromSensors()

    Sets interrupt reason to I2C interrupt and breaks waiting in game loop.
   ____________________________________________________________________________
*/
void interruptFromSensors() {
    interruptReason = INTERRUPT_REASON_I2C;
    loopInterrupt = true;
}

/* ____________________________________________________________________________

    void readDataFromSensors()

    Reads data from all devices and tries to parse them and pass them to 
    parseMessage function.
   ____________________________________________________________________________
*/
void readDataFromSensors() {
    uint8_t * GAME_I2Cbuffer = OS_Malloc(I2C_BUFFER_MAX_LENGTH);

    if (GAME_I2Cbuffer == NULL) {
        Trace(1,"Couldn't allocate enough memory for I2C buffer.");
        return;
    }

    uint8_t lastReceivedCommand = 0;
    bool receivedAllData = false;
    
    while (!receivedAllData) {
        receivedAllData = readMessageFromAllDevices(GAME_I2Cbuffer);
        Trace(1, "Parsing data: Command %x", GAME_I2Cbuffer[0]);
        if (lastReceivedCommand != GAME_I2Cbuffer[0]) { // Debounce commands - If two sensors register shot, read only from one.
            parseMessage(GAME_I2Cbuffer);

            if (GAME_I2Cbuffer[0] != NOINFO) {
                lastReceivedCommand = GAME_I2Cbuffer[0];
            }
        }
    };

    OS_Free(GAME_I2Cbuffer);
}

/* ____________________________________________________________________________

    void sendLoginMessage()

    Sends login message to socket server.
   ____________________________________________________________________________
*/
void sendLoginMessage() {
    uint8_t LOGIN_MESSAGE_BUFFER_SIZE = 20;
    char * loginMessageBuffer = OS_Malloc(LOGIN_MESSAGE_BUFFER_SIZE);

    if (loginMessageBuffer == NULL) {
        Trace(1, "Couldn't allocate login message buffer");
        return;
    }

    memset(loginMessageBuffer, 0, LOGIN_MESSAGE_BUFFER_SIZE);
    sprintf(loginMessageBuffer, LOGIN_MESSAGE, VEST_CODE);

    WriteWithCheck(loginMessageBuffer);

    OS_Free(loginMessageBuffer);
}

/* ____________________________________________________________________________

    void gameLoop()

    Processes all interrupts and game logic in one second long loop.
   ____________________________________________________________________________
*/
void gameLoop() {
    //Deal with possible interrupts first.
    if (interruptReason == INTERRUPT_REASON_I2C) {
        interruptReason = INTERRUPT_REASON_NONE;
        OS_Sleep(200); //Give some time to all sensors to read the value before sending.
        readDataFromSensors();
    }

    if (interruptReason == INTERRUPT_REASON_SHOOT) {
        interruptReason = INTERRUPT_REASON_NONE;
        shoot();
    }

    //Check if game is ready to be started.
    uint32_t currentTime = getTime(); 
    if (gameStartTime != 0) {
        if (gameStartTime < currentTime) {
            gameStartTime = 0;
            start();
        }
    }

    if (isStarted) {
        if (killTime != 0) {
            if (killTime + respawnPeriod < currentTime) {
                respawn();
            }
        }
        
        if (gameEndTime < currentTime) {
            stop();
        }
        sendGpsInfo();
    }

    gameRedrawDisplay();

    //Sleep for 1s. If interrupt comes, break the loop and update immediately. That's because interrupts cannot use semaphores, 
    //since OS_Sleep has to be called in Task context and game loop is called from Task context.
    int gameLoopCounter = 0;
    while (gameLoopCounter < 1000 && loopInterrupt == false) {
        OS_Sleep(1);
        gameLoopCounter++;
    }

    Trace(1, "GameLoopCounter: %d, loop interrupt %d", gameLoopCounter, loopInterrupt);

    loopInterrupt = false;
    
}

/* ____________________________________________________________________________

    void showGameStartsIn()

    Show information on display about how many seconds are left before 
    the game starts.
   ____________________________________________________________________________
*/
void showGameStartsIn() {
    setCursor(0, 0);
    sendString(GAME_STARTS_IN_MESSAGE);

    uint32_t currentTime = getTime(); 
    uint16_t seconds = gameStartTime - currentTime;

    setCursor(0, 1);

    char secondsRemaining[16];
    sprintf(secondsRemaining,"%5d %-10s", seconds, SECONDS_MESSAGE);
    sendStringL(secondsRemaining, 16);
}

/* ____________________________________________________________________________

    void showHealthAndAmmo()

    Show health and ammo count on display.
   ____________________________________________________________________________
*/
void showHealthAndAmmo() {
    char healthChars[4]; //Three letters + null terminator
    char ammoChars[4]; //Three letters + null terminator

    setCursor(0, 0);
    writeDisplay(0); //Write heart

    sprintf(healthChars,"%d", health);
    sprintf(ammoChars,"%3d", ammo); //Allign to right setting min width to 3
    
    setCursor(2, 0);
    sendString(healthChars); //Write health value

    setCursor(11, 0);
    sendString(ammoChars); //Write health value

    setCursor(15, 0);
    writeDisplay(1); //Write ammo symbol
}

/* ____________________________________________________________________________

    void showLoadingMessage()

    Show loading message on display.
   ____________________________________________________________________________
*/
void showLoadingMessage() {
    setCursor(0, 0);
    sendString(LOADING_MESSAGE);

    setCursor(6, 0);
    writeDisplay(0);
    
    setCursor(0, 1);
    sendString(LOADING_MESSAGE_ROW2);
}

/* ____________________________________________________________________________

    void showEndOfGameMessage()

    Show message that the game ended.
   ____________________________________________________________________________
*/
void showEndOfGameMessage() {
    setCursor(0, 0);
    sendString(END_OF_GAME_MESSAGE);

    setCursor(0, 1);
    sendString(END_OF_GAME_MESSAGE_ROW2);
}

/* ____________________________________________________________________________

    void showLoginFailedMessage()

    Show message that the vest couldn't login to the server.
   ____________________________________________________________________________
*/
void showLoginFailedMessage() {
    clear();  
    setCursor(0, 0);
    sendString(LOGIN_FAILED_MESSAGE);

    setCursor(0, 1);
    sendString(LOGIN_FAILED_MESSAGE_ROW2);
}

/* ____________________________________________________________________________

    void showReadyToStartMessage()

    Show message that the vest is ready to start the game.
   ____________________________________________________________________________
*/
void showReadyToStartMessage() {    
    setCursor(0, 0);
    sendString(READY_MESSAGE);

    setCursor(0, 1);
    sendString(READY_MESSAGE_ROW2);

    setCursor(strlen(READY_MESSAGE_ROW2), 1);
    sendString(VEST_CODE);
}

/* ____________________________________________________________________________

    void showRespawnMessage()

    Show message that the player is dead and how many seconds are left before
    the player is respawned.
   ____________________________________________________________________________
*/
void showRespawnMessage() {
    setCursor(0, 0);
    sendString(RESPAWN_IN_MESSAGE);
    
    uint32_t currentTime = getTime(); 
    uint32_t seconds = killTime + respawnPeriod - currentTime;
    
    setCursor(0, 1);
    char secondsRemaining[16];
    sprintf(secondsRemaining,"%5d %-10s", seconds, SECONDS_MESSAGE);
    sendStringL(secondsRemaining, 16);
}

/* ____________________________________________________________________________

    void redrawTimeLeft()

    Shows on display how much time is left before the game ends.
   ____________________________________________________________________________
*/
void redrawTimeLeft() {
    setCursor(0, 1);
    
    uint32_t currentTime = getTime(); 
    uint16_t seconds = gameEndTime - currentTime;
    uint8_t minutes = (gameEndTime - currentTime) / 60; //Find number of minutes

    seconds -= minutes * 60;                            //Subtract number of minutes * 60.
    
    char secondsRemaining[16];
    sprintf(secondsRemaining,"%s %dm %02ds ", TIME_REMAINING_MESSAGE, minutes, seconds);
    sendStringL(secondsRemaining, 16);
}

/* ____________________________________________________________________________

    void showShotBy()

    Shows on display who shot or killed current player.
   ____________________________________________________________________________
*/
void showShotBy() {
    setCursor(0, 0);
    if (isAlive()) {
        sendString(SHOT_BY_MESSAGE);
    } else {
        sendString(KILLED_BY_MESSAGE);
    }
    
    setCursor(0, 1);

    
    Player * shotByPlayer = findPlayerById(shotBy);
    
    if (shotByPlayer == NULL) {       //Fallback: Show only ID if you cannot find the user
        char killPlayerID[3];
        sprintf(killPlayerID,"%d", shotBy);
        sendString(killPlayerID);
        return;
    }

    sendString(shotByPlayer->username);
}

/* ____________________________________________________________________________

    void gameRedrawDisplay()

    Updates information that are shown on the display.
   ____________________________________________________________________________
*/
void gameRedrawDisplay() { 
    if (displayReinitializeCounter == DISPLAY_REINITILAIZE_INTERVAL) {
        begin();
        displayReinitializeCounter = 0;
    } else {
        displayReinitializeCounter++;
        clear();
    }

    if (endOfGameMessageShownCounter != -1) { // Show info that the game is over.
        showEndOfGameMessage();

        endOfGameMessageShownCounter++;
        if (endOfGameMessageShownCounter == QUICK_MESSAGE_TIMEOUT) {
            endOfGameMessageShownCounter = -1;
        }

        return;
    }

    if (isStarted == false && gameStartTime == 0) {     //Show code of the vest on the display.

        showReadyToStartMessage();
        return;
    }

    if (isStarted == false && gameStartTime != 0) {     //Show countdown to start of the game.
        showGameStartsIn();
        return;
    }
    
    if (isAlive() && shotBy == 0) {                     // usual screen showing ammo, health and time left
        showHealthAndAmmo();
        redrawTimeLeft();
        return;
    }
    
    if (shotBy != 0) {                                  // Screen showing who shot this player. After some time return back to showing ammo or respawn message.
        showShotBy();

        shotByMessageShownCounter++; 
        if (shotByMessageShownCounter == QUICK_MESSAGE_TIMEOUT) {
            if (isAlive()) {
                shotBy = 0;                             //We can null this value in here if the player is alive. If he is dead, it should be set until respawn.
            }

            shotByMessageShownCounter = 0;
        }
        return;
    }

    if (killTime != 0) {                                //Show when the player is going to respawn
        showRespawnMessage();
    }
}