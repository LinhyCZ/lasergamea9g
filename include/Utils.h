#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <time.h>

/* ____________________________________________________________________________

    Function Prototypes
   ____________________________________________________________________________
*/
uint32_t getTime();
bool startsWith(const char *pre, const char *str);

#endif