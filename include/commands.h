#ifndef COMMANDS_H
#define COMMANDS_H

/* ____________________________________________________________________________

    I2C commands
   ____________________________________________________________________________
*/
//A9G -> ARDUINO
#define HELLO           0x01
#define DEACTIVATE      0x02
#define ACTIVATE        0x03
#define OUTOFAMMO       0x04
#define RELOAD_COMPLETE 0x05
#define INIT            0x06


//ARDUINO -> A9G
#define HELLO_RESPONSE  0x81
#define SHOT            0x82
#define RELOAD          0x83
#define NOINFO          0xA0


/* ____________________________________________________________________________

    Socket commands
   ____________________________________________________________________________
*/
//Server -> Vest
#define SOCKET_PING_MESSAGE         "PING"
#define SOCKET_WELCOME_MESSAGE      "Welcome to lasergame"
#define PREPARE_MESSAGE             "PREP"
#define START_MESSAGE               "START"
#define STOP_MESSAGE                "STOP"
#define LOGINOK_MESSAGE             "LOGINOK"
#define LOGINERR_MESSAGE            "LOGINERR"

//Vest -> Server
#define ACK_MESSAGE                 "ACK_%d\n"
#define LOGIN_MESSAGE               "LOGIN_%s\n"
#define PREPARE_DONE_MESSAGE        "PREPAREDONE\n"
#define START_DONE_MESSAGE          "STARTDONE\n"
#define POSITION_MESSAGE            "POS_%f_%f\n"
#define KILLED_BY_SOCKET_MESSAGE    "SHOT_%d\n"

#endif