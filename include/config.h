#ifndef CONFIG_H
#define CONFIG_H

/* ____________________________________________________________________________

    Socket configuration
   ____________________________________________________________________________
*/

#define DNS_DOMAIN                      "linhy.cz"      //DNS domain of the lasergame server
#define SERVER_PORT                     13957           //Port of the lasergame server
#define RECEIVE_BUFFER_MAX_LENGTH       200             //Socket message buffer
#define SOCKET_RECONNECT_INTERVAL       15000           //Interval between reconnects on error

/* ____________________________________________________________________________

    Generic configuration
   ____________________________________________________________________________
*/
#define CHIP                            A9              //Define if the chip is A9 or A9G

#define VEST_CODE                       "TESTA9"        //Vest code 

//GSM configuration
#define APN                             "internet"      //APN name
#define NETWORK_USERNAME                EMPTY_STRING    //GSM network username
#define NETWORK_USERPASSWORD            EMPTY_STRING    //GSM network password

//I2C
#define I2C_PORT                        I2C3            //Define I2C pins. Defined in A9/A9G SDK

#define I2C_SENSOR_START_ADDRESS        10              //Sensors I2C start address
#define I2C_WEAPON_ADDRESS              18              //Weapon I2C address
#define MAX_SENSORS                     5               //Maximum number of sensors to scan for

// I2C LCD
#define I2C_LCD_ADDRESS                 0x27            //I2C address of the I2C display
#define I2C_LCD_COLS                    16              //Number of columns on I2C display
#define I2C_LCD_ROWS                    2               //Number of rows on I2C display

#define GPIO_DEBOUNCE_TIME              100             //GPIO debounce time

#define MAX_PLAYERS                     8               //Maximum number of players that can be in game

#define GPS_SEND_INTERVAL               10              //Approximate interval (in seconds) to send GPS coordinates to server
#define DISPLAY_REINITILAIZE_INTERVAL   30              //Approximate interval (in seconds) to reinitialize I2C display. Sometimes data get corrupted and only solution is to reset the display
#define QUICK_MESSAGE_TIMEOUT           10              //Approximate timeout (in game loops = seconds) to show quick messages (Shot by, ....)

/* ____________________________________________________________________________

    Task configuration - DO NOT CHANGE
   ____________________________________________________________________________
*/
#define MAIN_TASK_STACK_SIZE            (2048 * 2)
#define MAIN_TASK_PRIORITY              0
#define MAIN_TASK_NAME                  "Lasergame Main Task"

#define EVENT_TASK_STACK_SIZE            (2048 * 2)
#define EVENT_TASK_PRIORITY              1
#define EVENT_TASK_NAME                  "Lasergame Event Task"

/* ____________________________________________________________________________

    Constants - DO NOT CHANGE
   ____________________________________________________________________________
*/
//Chip dictionary
#define A9                              1
#define A9G                             2

//Generic constants
#define EMPTY_STRING                    ""

#endif