#ifndef I2C_COMM_H
#define I2C_COMM_H

#include "config.h"
#include "commands.h"
#include "Semaphore.h"

#include <stdio.h>
#include <api_hal_i2c.h>
#include <api_os.h>
#include <api_debug.h>

/* ____________________________________________________________________________

    Preprocessor-defined Symbols
   ____________________________________________________________________________
*/

/* Constants */
#define I2C_TIMEOUT_MS                  1000
#define I2C_BUFFER_MAX_LENGTH           10

#define I2C_SENSOR_SCAN_MAX_RETRIES     5
#define I2C_SENSOR_SCAN_DELAY           100

/* ____________________________________________________________________________

    Function Prototypes
   ____________________________________________________________________________
*/

void initializeI2C();
void scanForSensors();
I2C_Error_t sendByte(uint8_t byte, uint8_t address);
I2C_Error_t sendMessage(uint8_t* message, uint16_t length, uint8_t address);
I2C_Error_t readMessage(uint8_t *buffer, uint8_t address);
bool readMessageFromAllDevices(uint8_t *buffer);

void sendByteToWeapon(uint8_t data);
void sendMessageToWeapon(uint8_t * data, uint16_t len);

void broadcastByteToSensors(uint8_t data);
void broadcastByteToSensorsAndWeapon(uint8_t data);

void broadcastMessageToSensors(uint8_t * data, uint16_t len);
void broadcastMessageToSensorsAndWeapon(uint8_t * data, uint16_t len);

#endif