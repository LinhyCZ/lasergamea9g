#ifndef SOCKET_H
#define SOCKET_H

#include <stdio.h>
#include <api_socket.h>
#include <api_os.h>
#include <api_event.h>
#include <api_debug.h>

#include "config.h"
#include "commands.h"
#include "Semaphore.h"


/* ____________________________________________________________________________

    Function Prototypes
   ____________________________________________________________________________
*/
bool Connect(int * errorCode);
void Close();

uint8_t * getSocketSemaphore();
void writeInt(int val);
void writeCharArray(uint8_t * data);
void WriteWithCheck(uint8_t* data);

char * receive(API_Event_t* pEvent);

#endif //SOCKET_H