#ifndef SEMAPHORE_H
#define SEMAPHORE_H

#include <stdio.h>
#include <api_os.h>

/* ____________________________________________________________________________

    Function Prototypes
   ____________________________________________________________________________
*/
void CreateSemaphore(uint8_t * sem);
void ReleaseSemaphore(uint8_t * sem);
void WaitSemaphore(uint8_t * sem);

#endif