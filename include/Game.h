#ifndef GAME_H
#define GAME_H

#include <api_audio.h>
#include <api_gps.h>
#include "gps.h"
#include "gps_parse.h"

#include "Display.h"
#include "commands.h"
#include "Socket.h"
#include "Utils.h"

/* ____________________________________________________________________________

    Preprocessor-defined Symbols
   ____________________________________________________________________________
*/

#define LOADING_MESSAGE             "Ahoj!"
#define LOADING_MESSAGE_ROW2        "Nacitam.."

#define GAME_STARTS_IN_MESSAGE      "Hra zacina za:"

#define TIME_REMAINING_MESSAGE      "Zbyva:"

#define LOGIN_FAILED_MESSAGE        "Nemohl jsem se"
#define LOGIN_FAILED_MESSAGE_ROW2   "prihlasit."

#define READY_MESSAGE               "Pripraveno k hre"
#define READY_MESSAGE_ROW2          "Kod:"

#define RESPAWN_IN_MESSAGE          "Respawn za:"

#define SHOT_BY_MESSAGE             "Zasazen od: "
#define KILLED_BY_MESSAGE           "Zastrelen hracem"

#define END_OF_GAME_MESSAGE         "Konec hry."
#define END_OF_GAME_MESSAGE_ROW2    "Vrat se na start"

#define SECONDS_MESSAGE             "sekund"

#define INTERRUPT_REASON_NONE       0
#define INTERRUPT_REASON_I2C        1
#define INTERRUPT_REASON_SHOOT      2


/* ____________________________________________________________________________

    Structures and Datatypes
   ____________________________________________________________________________
*/

typedef struct _Player
{
    uint8_t playerId;
    uint8_t teamId;
    char username[10];
} Player;

/* ____________________________________________________________________________

    Function Prototypes
   ____________________________________________________________________________
*/
bool isLoggedIn();
void start();
void stop();
void respawn();
void initializeGame();
void gameLoop();

void shootInterrupt();
void interruptFromSensors();

void parseMessage(uint8_t * message);
void parseSocketMessage(char * message);

void showGameStartsIn();
void showHealthAndAmmo();
void showLoadingMessage();
void showReadyToStartMessage();
void showLoginFailedMessage();
void gameRedrawDisplay();

#endif